from sql_parse import validate

# Parses what the user types in, into a lis of words that are passed to the
# sql_parse.py file to be turned into a SQL query
query = "";

# example query:
query = "get gold from Montreal 1976"

# should look like this when it is sent to the backend
post_pre_parsing = ["get", "gold", "from", "Montreal,1976"]
final_query = "SELECT athletes.athlete, country, event, year, city FROM athletes LEFT JOIN medals ON athletes.athlete = medals.athlete WHERE medal = 'Gold';"

query = "get medal from BOLT,Usain"
post_pre_parsing = ["get", "medals", "from", "BOLT,Usain"]
final_query = "SELECT count(medal) FROM athletes LEFT JOIN medals on athletes.athlete = medals.athlete WHERE athletes.athlete = 'BOLT, Usain';"

# returns all gold medal athletes from montreal 1976

# split the data before and after the from
# between get and from is the data to collect
# after the from is the conditions

def pre_parse(user_input):
    data = user_input.split(" ")
    data[2] = ''
    for i in range(3, len(data)):
        if data[i][0] == '"':
            if validate('athletes.Athlete', data[i].upper() + ', ' + data[i+1]):
                data[2] = data[2] + '|' + data[i].upper() + ', ' + data[i+1]
            else:
                data[2] = data[2] + '|' + data[i] + ' ' + data[i+1]
        elif data[i][-1] == '"':
            pass
        else:
            data[2] = data[2] + '|' + data[i]
    data[3] = data[2][1:len(data[2])]
    #print(data[3])
    return data