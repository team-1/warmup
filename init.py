# This file initiates a connection the db, and allows the DB connection to be closed
import sqlite3
from sqlite3.dbapi2 import Connection, Cursor
from sql_parse import init, disconnect, run_query
import csv
import pandas as pd
import subprocess

def main_init():
  con: Connection
  cur: Cursor

  # init connection to database
  cur, con, db_did_exist = init()

  athletes = pd.read_csv('athletes.csv')
  medals = pd.read_csv('medals.csv')

  # overwrite current db if it already exists
  if db_did_exist:
    run_query("DROP TABLE athletes")
    run_query("DROP TABLE medals")

  athletes.to_sql('athletes', con)
  medals.to_sql('medals', con)
  

  return cur, con

def close():
  disconnect()
