# Main logic is in this file
from sqlite3.dbapi2 import Connection, Cursor
from init import main_init, close
from language_parse import pre_parse
from sql_parse import handle_front_query
import os
class Database():
    cur: Cursor
    con: Connection

    def __init__(self) -> None:
        self.cur = None
        self.con = None

    def run(self):
        while True:
            self.user_input()

    # get user input
    def user_input(self):
        user_input = input("Enter query: ")
        if user_input == "quit":
            quit()
        elif user_input == "load data":
                self.cur, self.con = main_init()
        elif (not os.path.isfile('olympics.db')) or (self.cur == None or self.con == None):
            print("Please run the load data command before continuing.")
        elif user_input == "help":
            print("You can retrieve any information contained in the Olympics database,\n"
                'Example: get Silver from "United States".\n'
                "Or you can retrieve the number of instances that something occurs in the table.\n"
                'Example: count Silver from "United States"\n'
                "Names have to be in quotation marks.\n"
                'Example: get Gender from "Bolt Usain"\n'
                "Lastly, athletes and medals tables must be entered in lowercase.\n"
                )
        else: 
            data = pre_parse(user_input)
            print(handle_front_query(data))

# main function
def main():
    # create database object
    db = Database()

    # run the database
    db.run()

main()
