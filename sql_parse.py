# "Backend" file - handles parsing the already parsed query into an actual SQL
# query

# Import sqlite3 to access our database file
import sqlite3
import os
import csv
#from typing import final

# Initalize the global variables for the connection and cursor
con = None
cur = None

def init():
    global con
    global cur

    # Connect to database
    if os.path.isfile('olympics.db'):
        db_does_exist = True
    else:
        db_does_exist = False
    con = sqlite3.connect('olympics.db')
    # Create cursor to make queries
    cur = con.cursor()

    return cur, con, db_does_exist
    
def disconnect():
    # Disconnect from database
    con.close()

def handle_front_query(data):
    # List of possible features
    features = [ "Medal", "athletes.Athlete", "Country", "Gender", "Event", "Year", "City", "Event_gender" ]
    
    # ~~~~~~~~~~~~ build query ~~~~~~~~~~~~
    # note that all queries will just have a join so everything is put together
    # for simplicity
    #query = "SELECT athletes.athlete, country, event, year, city FROM athletes LEFT JOIN medals ON athletes.athlete = medals.athlete "

    # take in query data, and structure it into an actual query
    query_type = ""
    query = ""
    
    # ~~~~~~~~~~ Adding SELECT clause ~~~~~~~~~~~
    # check the initial query type
    if data[0] == 'get':
        query_type = "get"
        query = "select "
    elif data[0] == 'count':
        query_type = 'count'
        query = "select Count("
    else:
        return "Invalid Query"

    # get further information for query
    info_to_get:str = data[1]
    info_to_get_type = ''
    validated = False
    for feature in features:
        if validate(feature, info_to_get):
            validated = True
            info_to_get_type = feature
            # Only display names of person with gender
            if validate('Gender', info_to_get):
                query = query + 'distinct athletes.Athlete, Gender'
            elif validate('Medal', info_to_get) and not query_type == 'count':
                query = query + 'distinct athletes.Athlete, Event, Medal, Year'
            elif info_to_get == 'City':
                query = query + 'distinct City'
            elif info_to_get == 'Year':
                query = query + 'distinct Year'                
            else:
                query = query + '*'#info_to_get_type
    if not validated:
        return "Invalid Query"
    if query_type == 'count':
        query = query + ")"  

    query = query + " from athletes left join medals on athletes.athlete=medals.athlete "

    # ~~~~~~~~ Adding WHERE clause ~~~~~~~~
    # if this is the case, then conditions have been applied
    if len(data) >= 4:
        conditions = data[3].split('|')
        # get ready for where conditions
        query = query + " where "
        query = query + info_to_get_type + '="' + info_to_get + '" '
        for condition in conditions:
            validated = False
            for feature in features:
                if validate(feature, condition):
                    validated = True
                    condition = condition.replace('"','')
                    query = query + 'and ' + feature + '="' + condition + '" '
            if not validated:
                return "Invalid Query"
    # if no conditions, set conditions to be None
    else:
        conditions = None
 
    # add final semicolon
    query += ";"
    
    # run final query
    #print(query)
    run_query(query)
    return ""
    
def run_query(query):
    # Run a given SQL query and print each row of results

    results = cur.execute(query)
    for row in results:
        print(row)

    return results


# Takes in an expected column and the value then validates if it exists in that column
def validate(column, value):
    # Perform a simple query to get results from this column with this value
    val_query = 'select * from medals left join athletes on medals.athlete=athletes.athlete where ' + column + '="' + value.replace('"','') + '";'
    results = cur.execute(val_query)
    # If there are 1 or more results, this ensures that data is contained in this column
    i = 0
    for row in results:
        i = i + 1
    if i > 0:
        return True
    else:
        return False